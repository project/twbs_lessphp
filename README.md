TWBS lessphp
============

A simple helper module for [TWBS](https://drupal.org/project/twbs),
handle [lessphp](http://leafo.net/lessphp/) library initialization.

### Key Features

-   Provide [drush
    make](https://raw.github.com/drush-ops/drush/master/docs/make.txt)
    file for library download
-   Confirm library successfully initialized with
    [hook\_requirements()](https://api.drupal.org/api/drupal/modules!system!system.api.php/function/hook_requirements/7)
-   All .less will automatically convert as .css before Drupal CSS
    aggregation

### Special Note

Since this module target for server-side .less support, by default it
only take action when Drupal's CSS aggregation enabled. For client-side
.less support, please refer to [TWBS
less.js](https://drupal.org/project/twbs_less) for more information.

### Author

-   Developed by [Edison Wong](http://drupal.org/user/33940).
-   Sponsored by [PantaRei Design](http://drupal.org/node/1741828).
