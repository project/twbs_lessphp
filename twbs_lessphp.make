api = 2
core = 7.x

; Libraries
libraries[lessphp][directory_name] = lessphp
libraries[lessphp][download][type] = file
libraries[lessphp][download][url] = http://leafo.net/lessphp/src/lessphp-0.4.0.tar.gz
libraries[lessphp][type] = libraries
